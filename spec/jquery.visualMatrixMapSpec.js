jasmine.getFixtures().fixturesPath = '/spec/javascripts/fixtures';
jasmine.getJSONFixtures().fixturesPath = '/spec/javascripts/fixtures/json';

describe('VisualMatrixMap', function () {
     beforeEach(function() {
            
             loadFixtures('rootFixture.html');
            $("#testObject").visualMatrixMap();
         });
    it('should generate dom infrastructure', function () {
       
        
        expect($('#testObject')).toBe('div#testObject');
        expect($('#testObject')).toHaveClass('VisualMatrixMap');
        expect($('#testObject')).toContain('canvas.visualize');
        expect($('#testObject')).toContain('div.interactive');
        expect($('#testObject')).toHaveData('sid');
       
       expect( $("#testObject").visualMatrixMap("data")["size"]["width"]).toBe(200);
       expect( $("#testObject").visualMatrixMap("data")["size"]["height"]).toBe(100);
       
       expect( $("#testObject").visualMatrixMap("data.size")["width"]).toBe(200);
       expect( $("#testObject").visualMatrixMap("data.size")["height"]).toBe(100);
       
       expect( $("#testObject").visualMatrixMap("data.size.width")).toBe(200);
       expect( $("#testObject").visualMatrixMap("data.size.height")).toBe(100);       
       
       expect( $("#testObject").visualMatrixMap("data")["tiles"].length).toBe(200);
       expect( $("#testObject").visualMatrixMap("data")["tiles"][0].length).toBe(100);
       
       expect( $("#testObject").visualMatrixMap("data.tiles").length).toBe(200);
       expect( $("#testObject").visualMatrixMap("data.tiles")[0].length).toBe(200);
        
       expect( $("#testObject").visualMatrixMap("data")["tilesDef"].length).toBe(2);
       expect( $("#testObject").visualMatrixMap("data.tilesDef").length).toBe(2);
       
       
       
       
       
       
       
       
    });
    
 
    
    


});