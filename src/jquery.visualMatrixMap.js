// jQuery.visualMatrixMap v0.1.0
// 5/2013 by Krzysztof Bakun (bakun.biz)
// License http://creativecommons.org/licenses/by-nc/3.0/legalcode
// To use: $('#target-div').visualMatrixMap(options)
//

(function ($) { 
    
    
    
    
    
    
    $.fn.visualMatrixMap = function ( options) {
        var Data = {
            "sprites":[]
        };
        var vectorX = 0;
        var vectorY = 0;
        var speedX = 0;
        var speedY = 0;
        var GUID = "";
        var tileW = 250 ;
        var tileH = 250;
        var scaleMutitiply = 1;
        var defaults = {
            "size": [
            {
                "width":200,
                "height":100
            }],
            "tilesDef":[
            {
                "name":"ziemia",
                "canBuild":"true",
                "speed":"0",
                "energy":"0",
                "css":"background-colour:green;"
            },
            {
                "name":"skały",
                "canBuild":"false",
                "speed":"0",
                "energy":"0",
                "css":"background-colour:red;"
            }
            ]
        
        };
       
        //console.log(options,options);
        
        options = $.extend(defaults, options);
        //console.log("after extends",options,options);
        
        return this.each(function (index, obj) {
            generateGUID();
            generateHtml(obj,index);
        
        
        });
        function generateGUID(){
            var d = new Date();
            GUID = "visualMatrixMap_"+d.getTime()+"_"+d.getUTCMilliseconds();
        
        }
        function generateHtml(obj,index){
            // create an array of assets to load
            var assetsToLoader = [ "spriteSheet.json"];

            // create a new loader
            var loader = new PIXI.AssetLoader(assetsToLoader);

            // use callback
            loader.onComplete = onAssetsLoaded

            //begin load
            loader.load();
            function onAssetsLoaded()
            { 
                $(obj).empty();
                $(obj).addClass("visualMatrixMap");
                var stageWidth =  $(obj).width();
                var stageHeight = $(obj).height();
                var stage = new PIXI.Stage(0x66FF99,true);
                var animate = function(){
                
                    refresh();
                    renderer.render(stage);

                    requestAnimationFrame(animate);
                }
                var renderer = PIXI.autoDetectRenderer(stageWidth, stageHeight);
                $(obj).append(renderer.view);
                //document.body.appendChild(renderer.view);
                var container = new PIXI.Graphics();
                var topContainer = new PIXI.Graphics();
                obj.container = container;
                // container.drawCircle(0, 0, 100);
                //container.drawRect(0,0,100,100);
           
                for(var j=0;j<200;j++){
                    Data.sprites.push([]);
                    for(var i=0;i<200;i++){
                        var sprite = {
                            "onStage":false,
                            position:{
                                x:i*tileW,
                                y:j*tileH
                            },
                            idx: [j,i]
                        };
                        Data.sprites[j].push(sprite);
                    //container.addChild(sprite);
                    }
                }
                dragable(container);
                topContainer.addChild(container);
                stage.addChild(topContainer);
                requestAnimationFrame(animate);
    
                var closein = function ( valueStart, valueTarget) {
                    return (29*valueStart + valueTarget)/30;
                };
    
                var refresh = function (){
                    
                    container.position.x += speedX;
                    container.position.y += speedY;
                    var speed = Math.sqrt(speedX*speedX) +Math.sqrt(speedY*speedY);
                    
                    topContainer.scale.x = scaleMutitiply*(1 - speed*.003);
                    topContainer.scale.y = scaleMutitiply*(1 - speed*.003); 
                    
                    speedX = closein(speedX ,vectorX);
                    speedY = closein(speedY ,vectorY);
                    //remove from stage
                    if(obj.container.children.length>0){
                        for(var i=0;i<obj.container.children.length;i++){
                            var o = obj.container.children[i];
                            if(!isOnStage(o.position.x,o.position.y,tileW,tileH)){
                                obj.container.removeChild(o);
                                Data.sprites[o.idx[0]][o.idx[1]].onStage=false;
                            }
                        }
                    }
                    //add to stage
                    var left = Math.floor(-obj.container.position.x/tileW);
                    var top = Math.floor(-obj.container.position.y/tileH);
                    for(var j=top;j<Math.min(Data.sprites.length,top+stageHeight/tileH+3);j++){ 
                        j = Math.floor(Math.max(0,Math.min(Data.sprites.length-1,j)));
                        for(i=left;i<Math.min(Data.sprites[j].length,left+stageWidth/tileW+3);i++){
                       
                            i = Math.floor(Math.max(0,Math.min(Data.sprites[j].length-1,i)));
                            o = Data.sprites[j][i];
                            if(!o.onStage && isOnStage(o.position.x,o.position.y,tileW,tileH)){
                                var id = Math.floor(1+Math.random()*11).toString();
                                var texture = PIXI.Texture.fromFrame(id +".png");
                                var sprite =  new PIXI.Sprite(texture);
                                sprite.position.x = o.position.x;
                                sprite.position.y = o.position.y;
                                //sprite.scale.x = 5;
                                //sprite.scale.y = 3;
                                //sprite.alpha = Math.random();
                                sprite.idx = o.idx;
                                o.onStage = true;
                                 
                            
                                obj.container.addChild (sprite);
                                o.onStage=true;
                            }
                        }
                    }
                    //console.log(obj.container.children.length);
                    function isOnStage(x,y,w,h){
                        //                    console.log(x,obj.container.position.x,y,obj.container.position.x+x>0-w,
                        //                    obj.container.position.x+x<stageWidth ,
                        //                        obj.container.position.y+y>0-h ,
                        //                        obj.container.position.y+y<stageHeight+h);
                        var corectX = stageWidth*(1 - topContainer.scale.x)*2;
                        var corectY = stageWidth*(1 - topContainer.scale.y)*2; 
                        return (obj.container.position.x+x>0-w &&
                            obj.container.position.x+x<stageWidth+w+corectX &&
                            obj.container.position.y+y>0-h &&
                            obj.container.position.y+y<stageHeight+h+corectY);
                    }
                }
                $(document).keydown(function(e) {
                    var step = 50;
                    console.log (e.keyCode ) ;
                    if(e.keyCode==38){
                        //up
                        vectorY = -step;
                        vectorX = 0;
                    }
                    if(e.keyCode==40){
                        //down
                        vectorY = step;
                        vectorX = 0;
                    }
                    if(e.keyCode==37){
                        //left
                        vectorX = -step;
                        vectorY = 0;
                    }
                    if(e.keyCode==39){
                        //right
                        vectorX = step;
                        vectorY = 0 ;
                    }
                    if(e.keyCode==187){
                        //=
                        scaleMutitiply+=.01;
                        
                    }
                    if(e.keyCode==189){
                        //-

                        scaleMutitiply -= .01;
                       
                    }
                
                });
                $(document).keyup(function(e) {
                    vectorY = 0;
                    vectorX = 0;
                });
            }

            
        }
       
        
        function dragable(sprite){
            sprite.setInteractive(true);
            sprite.mousedown = sprite.touchstart = function(data)
            {
                this.data = data;
                this.alpha = 0.9;
                this.dragging = true;
                
                this.startPos = this.data.getLocalPosition(this);
                
            };
		
            // set the events for when the mouse is released or a touch is released
            sprite.mouseup = sprite.mouseupoutside = sprite.touchend = sprite.touchendoutside = function(data)
            {
                this.alpha = 1
                this.dragging = false;
                this.data = null;
                
                vectorX = 0;
                vectorY = 0;
            };
		
            // set the callbacks for when the mouse or a touch moves
            sprite.mousemove = sprite.touchmove = function(data)
            {
                if(this.dragging)
                {
                    var newPosition = this.data.getLocalPosition(this.parent);
                    
                    this.position.x = newPosition.x-this.startPos.x;
                    this.position.y = newPosition.y-this.startPos.y;

                }
            }
        }

    };




})(jQuery);